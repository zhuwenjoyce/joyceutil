package com.joyce.util.valid;

import javax.imageio.ImageIO;

import com.joyce.util.StrConstantUtil;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Random;

/**
 * 图片验证码工具类
 * Created by yukui on 2017/1/11.
 */
public class ImgVerifyCodeUtil {
	
    private static Random random = new Random();

    /**
     * 使用系统默认字符源生成验证码
     *
     * @param verifySize 验证码长度
     * @return
     */
    public static String generateVerifyCode(int verifySize) {
        return generateVerifyCode(verifySize, StrConstantUtil.VERIFY_CODES);
    }


    /**
     * 使用指定字符源生成指定长度的验证码
     *
     * @param verifySize 验证码长度
     * @param sources    验证码字符源，如果为空，则使用系统默认验证码字符源
     * @return 返回指定长度的指定验证码字符范围的验证码字符
     */
    public static String generateVerifyCode(int verifySize, String sources) {
        if (sources == null || sources.length() == 0) {
            sources = StrConstantUtil.VERIFY_CODES;
        }
        int codesLen = sources.length();
        Random rand = new Random(System.currentTimeMillis());
        StringBuilder verifyCode = new StringBuilder(verifySize);
        for (int i = 0; i < verifySize; i++) {
            verifyCode.append(sources.charAt(rand.nextInt(codesLen - 1)));
        }
        return verifyCode.toString();
    }

    /**
     * 输出指定验证码字符的图片流
     *
     * @param width      指定验证码图片宽度
     * @param height     指定验证码图片高度
     * @param output     指定验证码输出流
     * @param code       指定验证码字符串
     * @param formatName 指定图片格式，如jpg,png等
     * @throws IOException
     */
    public static void outputImage(int width, int height, OutputStream output, String code, String formatName) throws IOException {
        int verifySize = code.length();
        //imageType设置为rgb类型
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        //获得画笔
        Graphics2D graphics2d = image.createGraphics();
        graphics2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        //设置背景色，纯色
        Color c = getRandColor(200, 250);
        graphics2d.setColor(c);
        graphics2d.fillRect(0, 2, width, height); 

        //绘制25条干扰线
        graphics2d.setColor(getRandColor(160, 200));// 设置干扰线的颜色
        for (int i = 0; i < 25; i++) {
            int x = random.nextInt(width);
            int y = random.nextInt(height);
            int xl = random.nextInt(width);
            int yl = random.nextInt(height);
            graphics2d.drawLine(x, y, x + xl + 40, y + yl + 20);
        }

        // 添加噪点
        float yawpRate = 0.05f;// 噪声率
        int area = (int) (yawpRate * width * height);
        for (int i = 0; i < area; i++) {
            int x = random.nextInt(width);
            int y = random.nextInt(height);
            image.setRGB(x, y, random.nextInt(255)); //设置噪点随机颜色
        }

        //在图片中填充验证码
        graphics2d.setColor(getRandColor(100, 160));
        int fontSize = height - 4;
        Font font = new Font("Algerian", Font.ITALIC, fontSize);
        graphics2d.setFont(font);
        char[] chars = code.toCharArray();
        //随机角度倾斜验证码字符
        for (int i = 0; i < verifySize; i++) {
            AffineTransform affine = new AffineTransform();
            affine.setToRotation(Math.PI / 4 * random.nextDouble() * (random.nextBoolean() ? 1 : -1), (width / verifySize) * i + fontSize / 2, height / 2);
            graphics2d.setTransform(affine);
            graphics2d.drawChars(chars, i, 1, ((width - 10) / verifySize) * i + 5, height / 2 + fontSize / 2 - 10);
        }

        graphics2d.dispose();
        ImageIO.write(image, formatName, output);
    }

    /**
     * @param start   颜色取值起始值，必须小于等于255，否则无效
     * @param end     颜色取值结束值，必须小于等于255，否则无效
     * @return Color  返回指定范围内的随机颜色Color对象
     */
    private static Color getRandColor(int start, int end) {
        int r = start + random.nextInt(end - start);
        int g = start + random.nextInt(end - start);
        int b = start + random.nextInt(end - start);
        return new Color(r, g, b);
    }

}
