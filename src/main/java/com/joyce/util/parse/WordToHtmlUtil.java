package com.joyce.util.parse;

import com.joyce.util.StrConstantUtil;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.converter.WordToHtmlConverter;
import org.apache.poi.xwpf.converter.xhtml.XHTMLConverter;
import org.apache.poi.xwpf.converter.xhtml.XHTMLOptions;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import java.io.*;

/**
 * word文档转html
 */
public class WordToHtmlUtil {
	
	/**
	 * word文档里的内容转换为html内容
	 * @param sourceFileName doc文件流
	 * @param targetFileName  html文件路径
	 *  @param encoding  文件格式 一般是UTF-8
	 */
    // doc转换为html
	public  void docToHtml(File sourceFileName,String targetFileName,String encoding) throws IOException, ParserConfigurationException, TransformerException {

    		 HWPFDocument wordDocument = new HWPFDocument(new FileInputStream(sourceFileName));
    	        Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
    	        WordToHtmlConverter wordToHtmlConverter = new WordToHtmlConverter(document);
    	        wordToHtmlConverter.processDocument(wordDocument);
    	        Document htmlDocument = wordToHtmlConverter.getDocument();
    	        DOMSource domSource = new DOMSource(htmlDocument);
    	        StreamResult streamResult = new StreamResult(new File(targetFileName));
    	        TransformerFactory tf = TransformerFactory.newInstance();
    	        Transformer serializer = tf.newTransformer();
    	        serializer.setOutputProperty(OutputKeys.ENCODING, encoding );
				//添加额外的空格时输出结果树,必须是 或者 不是
    	        serializer.setOutputProperty(OutputKeys.INDENT, StrConstantUtil.YES);
                //TEXT_TYPE_HTML 文本格式为  "xml" | "html" | "text"
    	        serializer.setOutputProperty(OutputKeys.METHOD, StrConstantUtil.TEXT_TYPE_HTML);
    	        serializer.transform(domSource, streamResult);
    }

	/**
	 *  word文档里的内容转换为html内容，仅支持xlsx版本excel
	 * @param sourceFileName doc文件流
	 * @param targetFileName  html文件路径
	 * @param encoding  文件编码格式
	 */
    public void docxToHtml(File sourceFileName,String targetFileName,String encoding) throws IOException {
        OutputStreamWriter outputStreamWriter = null;
        try {
			XWPFDocument document = new XWPFDocument(new FileInputStream(sourceFileName));
			XHTMLOptions options = XHTMLOptions.create();
			outputStreamWriter = new OutputStreamWriter(new FileOutputStream(targetFileName), encoding);
			XHTMLConverter xhtmlConverter = (XHTMLConverter) XHTMLConverter.getInstance();
			xhtmlConverter.convert(document, outputStreamWriter, options);
		}finally {
				if (outputStreamWriter != null) {
	                outputStreamWriter.close();
	            }
        }
    }
}