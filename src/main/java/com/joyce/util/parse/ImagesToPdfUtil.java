package com.joyce.util.parse;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * 图片装PDF
 */
public class ImagesToPdfUtil {

	/**
	 * pdf一页显示一张图片信息
	 *
	 * @param imagePath
	 *            图片地址
	 * @param pdfPath
	 *            pdf存放地址
	 * @throws IOException
	 * @throws DocumentException
	 * @throws Exception
	 */
	public void imagesToPdfOne(String imagePath, String pdfPath)
			throws IOException, DocumentException {

		BufferedImage img = ImageIO.read(new File(imagePath));
		File file = new File(pdfPath);
		if (!file.exists()) {
			file.createNewFile();
		}
		FileOutputStream fos = new FileOutputStream(pdfPath);
		// 设置图片大小，高度，宽度
		Document doc = new Document(PageSize.A4, 0, 0, 0, 0);
		doc.setPageSize(new Rectangle(img.getWidth(), img.getHeight()));
		Image image = Image.getInstance(imagePath);
		PdfWriter.getInstance(doc, fos);
		doc.open();
		doc.add(image);
		doc.close();

	}

	/**
	 * pdf多页显示图片信息，一页pdf显示一张图片
	 *
	 * @param imagePath
	 *            图片地址
	 * @param pdfPath
	 *            pdf存放地址
	 * @param imageMore
	 *            截取图片存放地址
	 * @param sumPage 生成pdf的页数
	 * @param state  截取图片是否删除，true删除，false保留
	 * @throws IOException
	 * @throws DocumentException
	 * @throws Exception
	 */
	public void imagesToPdfMore(String imagePath, String pdfPath,
								String imageMore,  int sumPage, boolean state) throws IOException,
			DocumentException {

		BufferedImage img = ImageIO.read(new File(imagePath));
		File file = new File(pdfPath);
		if (!file.exists()) {
			file.createNewFile();
		}
		int totalPage = img.getHeight() / 14300;
		int sum= img.getHeight() / sumPage;
		// 实例截取图片对象
		ParseImagesUtil imageCut = new ParseImagesUtil();
		String imagePathString = imageCut.cut(imagePath, imageMore,0, 0, img.getWidth(), sum);
		Image image = Image.getInstance(imagePathString);
		FileOutputStream fos = new FileOutputStream(pdfPath);
		Document doc = new Document(PageSize.A4, 0, 0, 0, 0);
		doc.setPageSize(new Rectangle(img.getWidth(), sum));
		PdfWriter.getInstance(doc, fos);
		doc.open();
		doc.add(image);
		// 根据状态判断是否删除 截图的图片，true删除，false保留
		imageState(state, imagePathString);
		for (int i = 2; i <= sumPage; i++) {
			imageCut = new ParseImagesUtil();
			String imagePathTwo="";
			int cesum=sum * (i - 1);
			int countsum=cesum - 1;
			doc.newPage();
			if (img.getHeight() - countsum >= sum || totalPage == 0) {
				imagePathTwo=imageCut.cut(imagePath, imageMore,0, cesum, img.getWidth(), sum);
			} else {
				imagePathTwo=imageCut.cut(imagePath, imageMore,0,countsum, img.getWidth(), img.getHeight() - countsum);
			}
			image = Image.getInstance(imagePathTwo);
			doc.add(image);
			// 根据状态判断是否删除 截图的图片，true删除，false保留
			imageState(state, imagePathTwo);
		}
		doc.close();
	}

	/**
	 * 是否删除图片
	 *
	 * @param state
	 *            状态值
	 * @param imagePath
	 *            图片路径
	 */
	private void imageState(boolean state, String imagePath) {
		if (state) {
			File file = new File(imagePath);
			if (file.exists()) {
				file.delete();
			}
		}
	}
}
