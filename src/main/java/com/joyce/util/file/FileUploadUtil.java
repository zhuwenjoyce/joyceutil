package com.joyce.util.file;

import com.joyce.util.StrConstantUtil;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.lang.StringUtils;

import javax.servlet.http.HttpServletRequest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by 滕少君 on 2017/2/15.
 * 文件上传工具类
 */
public class FileUploadUtil {

    /**
     * 文件上传方法
     * @param savePath 文件保存目录
     * @param encoding 编码
     * @param request
     * @return 上传结果
     */
    @SuppressWarnings("unchecked")
	public static void upload(String savePath,String encoding,HttpServletRequest request) throws IOException, FileUploadException {
        File file = new File(savePath);
        //判断上传文件的保存目录是否存在，不存在创建
        if (!file.exists() && !file.isDirectory()){
            file.mkdir();
        }
            //1、创建一个DiskFileItemFactory工厂
            DiskFileItemFactory factory = new DiskFileItemFactory();
            //2、创建一个文件上传解析器
            ServletFileUpload upload = new ServletFileUpload(factory);
            //解决上传文件名的中文乱码
            upload.setHeaderEncoding(encoding);
            //3、判断提交上来的数据是否是上传表单的数据,按照传统方式获取数据
            if(!ServletFileUpload.isMultipartContent(request));
            //4、使用ServletFileUpload解析器解析上传数据，解析结果返回的是一个List<FileItem>集合，每一个FileItem对应一个Form表单的输入项
            List<FileItem> list = upload.parseRequest(request);
            for(FileItem item : list){
                if(!item.isFormField()){
                    String filename = item.getName();
                    if(StringUtils.isBlank(filename)){
                        continue;
                    }
                    //注意：不同的浏览器提交的文件名是不一样的，有些浏览器提交上来的文件名是带有路径的，如：  c:\a\b\1.txt，而有些只是单纯的文件名，如：1.txt
                    //处理获取到的上传文件的文件名的路径部分，只保留文件名部分
                    filename = filename.substring(filename.lastIndexOf(StrConstantUtil.REVERSE__SLANT)+1);
                    InputStream in = item.getInputStream();
                    FileOutputStream out = new FileOutputStream(savePath + StrConstantUtil.REVERSE__SLANT + filename);
                    //定义数据缓冲区
                    byte buffer[] = new byte[1024];
                    int len = 0;
                    while((len=in.read(buffer))>0){
                        out.write(buffer, 0, len);
                    }
                    
                    //关闭输入输出流
                    try{
                    	if(out != null){
                    		out.close();
                    	}
                    }finally{
                    	try{
                    		if(in != null){
                        		in.close();
                        	}
                    	}finally{
                    		//删除处理文件上传时生成的临时文件
                            item.delete();
                    	}
                    }
                    
                    
                 }
            }
     }
}
