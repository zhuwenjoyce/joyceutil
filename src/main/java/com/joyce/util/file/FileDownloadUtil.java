package com.joyce.util.file;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import com.joyce.util.StrConstantUtil;

/**
 * 文件下载辅助类
 * @author ZhuWen
 *
 */
public class FileDownloadUtil {

    /**
     * 浏览器客户端下载一个附件
     * @param attachmentPath  附件所在路径
     * @param attachmentName  附件名字
     * @param httpServerResponse  web容器的response对象
     * @throws IOException
     */
    public void downloadAttachment(
            String attachmentPath,String attachmentName,HttpServletResponse httpServerResponse) throws IOException {
        //获得附件的输入流
        BufferedInputStream dis = null;
        //获得response的输出流
        BufferedOutputStream fos = null;
        try {
            String fullFilePath = attachmentPath+ StrConstantUtil.BACK_SLANT +attachmentName;
            File attachmentFile = new File(fullFilePath);
            // 判断附件是否物理存在
            if (!attachmentFile.exists()) {
                throw new FileNotFoundException(fullFilePath);
            }
            // 给下载附件设置原来的下载名字，用于用户下载看到的附件名字
            httpServerResponse.setHeader(StrConstantUtil.RESPONSE_HEADER_CONTENT_DISPOSITION, StrConstantUtil.RESPONSE_HEADER_CONTENT_DISPOSITION_FILENAME
                    + new String(attachmentName.getBytes(StrConstantUtil.CHARSET_UTF8), StrConstantUtil.CHARSET_ISO8859));
            httpServerResponse.setHeader(StrConstantUtil.RESPONSE_HEADER_CONTENT_LENGTH,
                    String.valueOf(attachmentFile.length()));

            dis = new BufferedInputStream(new FileInputStream(attachmentFile));
            fos = new BufferedOutputStream(httpServerResponse.getOutputStream());
            // 设置读取缓冲区
            byte[] buff = new byte[2048];
            int bytesRead;
            while ((bytesRead = dis.read(buff, 0, buff.length)) != -1) {
                fos.write(buff, 0, bytesRead);
            }
            // 刷新缓冲区
            fos.flush();
        }finally {
            //此处不catch住异常，统统抛出，由调用此方法者自行做后续处理
            try {
                if (fos != null) {
                    fos.close();
                }
            } finally {
                if (dis != null){
                    dis.close();
                }
            }

        }
    }
}
