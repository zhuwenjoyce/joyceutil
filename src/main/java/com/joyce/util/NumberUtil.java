package com.joyce.util;


import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.joyce.util.StrConstantUtil;

/**
 * 数字工具类
 */
public class NumberUtil {

	/**
	 * @param begin 随机数开始最小数字（包含该数）
	 * @param end 随机数最大数字（不包含该数）
	 * @param size 指定产生随机数的个数
	 * @return 返回数组，里面包含size个不重复随机整数
	 */
	public static Integer[] randomInteger(int begin, int end, int size) {
		// 加入逻辑判断，确保begin<end并且size不能大于该表示范围
		if (begin >= end || (end - begin) < size) {
			return null;
		}

		Random ran = new Random();
		Set<Integer> set = new HashSet<Integer>();
		while (set.size() < size) {
			set.add(begin + ran.nextInt(end - begin));
		}
		return set.toArray(new Integer[size]);
	}

	/**
	 * 判断String是否是整数
	 */
	public static boolean isInteger(String s) {
		if (StringUtils.isNotBlank(s)){
			return s.matches(StrConstantUtil.REG_INTEGER);
		}else{
			return false;
		}
	}

	/**
	 * <pre>
	 * 判断字符串是否是浮点数
	 * 匹配规则：必须含有小数点的浮点数
	 * 10.00  = true 
	 * 9.0    = true 
	 * 99     = false
	 * 8.     = false 
	 * .9     = false 
	 * </pre>
	 */
	public static boolean isDouble(String value) {
		if (StringUtils.isNotBlank(value)){
			return value.matches(StrConstantUtil.REG_DOUBLE);
		}else{
			return false;
		}
	}

	/**
	 * 判断字符串是否是数字,整数或浮点数
	 */
	public static boolean isNumber(String value) {
		return isInteger(value) || isDouble(value);
	}

	/**
	 * 是否是质数
	 * @param n
	 * @return true / false
	 */
	public static boolean isPrimes(int n) {
		for (int i = 2; i <= Math.sqrt(n); i++) {
			if (n % i == 0) {
				return false;
			}
		}
		return true;
	}
	
	public static String toString(Object obj){
		return String.valueOf(obj);
	}

	/**
	 * 对数组进行排序：升序
	 * @param array
	 */
	public static void sortAscending(int[] array) {
		int temp = 0;
		for (int i = 0; i < array.length; i++) {
			for (int j = i; j < array.length; j++) {
				if (array[i] > array[j]) {
					temp = array[i];
					array[i] = array[j];
					array[j] = temp;
				}
			}
		}
	}

	/**
	 * 对数组进行排序：降序
	 * @param array
	 */
	public static void sortDescending(int[] array) {
		int temp = 0;
		for (int i = 0; i < array.length; i++) {
			for (int j = i; j < array.length; j++) {
				if (array[i] < array[j]) {
					temp = array[i];
					array[i] = array[j];
					array[j] = temp;
				}
			}
		}
	}

	/**
	 * 阶乘
	 * 
	 * @param n
	 * @return n次方
	 */
	public static int factorial(int n) {
		if (n == 1) {
			return 1;
		}
		return n * factorial(n - 1);
	}

	/**
	 * 平方根算法
	 * 
	 * @param x
	 * @return x的平方根
	 */
	public static long sqrt(long x) {
		long y = 0;
		long b = (~Long.MAX_VALUE) >>> 1;
		while (b > 0) {
			if (x >= y + b) {
				x -= y + b;
				y >>= 1;
				y += b;
			} else {
				y >>= 1;
			}
			b >>= 2;
		}
		return y;
	}

	/**
	 * 求m和n的最大公约数
	 */
	public static int maxCommonDivisor(int m, int n) {
		while (m % n != 0) {
			int temp = m % n;
			m = n;
			n = temp;
		}
		return n;
	}

	/**
	 * 求两数的最小公倍数
	 */
	public static int minCommonMultiple(int m, int n) {
		return n == 0? 0  :m * n / maxCommonDivisor(m, n);
	}

	/**
	 * 递归求两数的最大公约数
	 */
	public static int recursionMaxCommonDivisor(int m, int n) {
		if (m % n == 0) {
			return n;
		} else {
			return recursionMaxCommonDivisor(n, m % n);
		}
	}

}
