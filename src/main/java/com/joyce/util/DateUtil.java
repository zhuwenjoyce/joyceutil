package com.joyce.util;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 日期时间工具类
 */
public class DateUtil {

	/**
	 * @param dateFormat 指定格式
	 * @return 返回指定格式的当前时间字符串
	 */
	public static String formatCurrentDate(String dateFormat) {
		return new SimpleDateFormat(dateFormat).format(new Date());
	}

	/**
	 * @param date 指定日期
	 * @param dateFormat 指定格式
	 * @return 返回指定日期的指定格式字符串
	 */
	public static String formatToString(Date date, String dateFormat) {
		return new SimpleDateFormat(dateFormat).format(date);
	}
	
	/**
	 * 
	 * @param dateString 日期字符串
	 * @param dateFormat 日期格式
	 * @return 返回日期
	 * @throws ParseException 日期字符串解析错误
	 */
	public static Date parseToDate(String dateString,String dateFormat) throws ParseException{
		return new SimpleDateFormat(dateFormat).parse(dateString);
	}

	/**
	 * 
	 * @param dateString 日期字符串,格式必须为 yyyy-MM-dd
	 * @return yyyy年MM月dd日  举例： 二〇一七年六月五日
	 */
	public static String toChineseDate(String dateString) {
		String result = "";
		String[] cnDate = new String[] { "〇", "一", "二", "三", "四", "五", "六",
				"七", "八", "九" };
		String ten = "十";
		String[] dateStr = dateString.split("-");
		for (int i = 0; i < dateStr.length; i++) {
			for (int j = 0; j < dateStr[i].length(); j++) {
				String charStr = dateStr[i];
				String str = String.valueOf(charStr.charAt(j));
				if (charStr.length() == 2) {
					if (charStr.equals("10")) {
						result += ten;
						break;
					} else {
						if (j == 0) {
							if (charStr.charAt(j) == '1')
								result += ten;
							else if (charStr.charAt(j) == '0')
								result += "";
							else
								result += cnDate[Integer.parseInt(str)] + ten;
						}
						if (j == 1) {
							if (charStr.charAt(j) == '0')
								result += "";
							else
								result += cnDate[Integer.parseInt(str)];
						}
					}
				} else {
					result += cnDate[Integer.parseInt(str)];
				}
			}
			if (i == 0) {
				result += "年";
				continue;
			}
			if (i == 1) {
				result += "月";
				continue;
			}
			if (i == 2) {
				result += "日";
				continue;
			}
		}
		return result;
	}

	/**
	 * @param date
	 * @return 当前日期是本年的第几周
	 */
	public static int getWeekOfYear(Date date) {
		Calendar c = Calendar.getInstance();
		c.setFirstDayOfWeek(Calendar.MONDAY);
		c.setMinimalDaysInFirstWeek(Calendar.SATURDAY);
		c.setTime(date);
		return c.get(Calendar.WEEK_OF_YEAR);
	}
	/**
	 * @param year
	 * @return 得到某一年周的总数
	 */
	public static int getTotalWeekNumOfYear(int year) {
		Calendar c = Calendar.getInstance();
		c.set(year, Calendar.DECEMBER, 31, 23, 50, 50);
		c.setFirstDayOfWeek(Calendar.MONDAY);
		c.setMinimalDaysInFirstWeek(Calendar.SATURDAY);
		return c.get(Calendar.WEEK_OF_YEAR);
	}

	/**
	 * 
	 * @param year 某年
	 * @param week 某周
	 * @return 得到某年某周的第一天
	 */
	public static Date getFirstDayOfWeek(int year, int week) {
		Calendar c = Calendar.getInstance();
		//初始化年月日
		c.set(Calendar.YEAR, year);
		c.set(Calendar.MONTH, Calendar.JANUARY);
		c.set(Calendar.DATE, 1);
		//增加天数
		c.add(Calendar.DATE, week * 7);
		//以周一作为一周的第一天
		c.setFirstDayOfWeek(Calendar.MONDAY);
		//一周一共有7天
		c.setMinimalDaysInFirstWeek(Calendar.SATURDAY);
		//获得该周的第一天
		c.set(Calendar.DAY_OF_WEEK, c.getFirstDayOfWeek()); 
		return c.getTime();
	}

	/**
	 * @param year 某年
	 * @param week 某周
	 * @return 得到某年某周的最后一天
	 */
	public static Date getLastDayOfWeek(int year, int week) {
		Calendar c = Calendar.getInstance();
		//初始化年月日
		c.set(Calendar.YEAR, year);
		c.set(Calendar.MONTH, Calendar.JANUARY);
		c.set(Calendar.DATE, 1);
		//增加天数
		c.add(Calendar.DATE, week * 7);
		//以周一作为一周的第一天
		c.setFirstDayOfWeek(Calendar.MONDAY);
		//一周一共有7天
		c.setMinimalDaysInFirstWeek(Calendar.SATURDAY);
		//获得该周的最后一天
		c.set(Calendar.DAY_OF_WEEK, c.getFirstDayOfWeek()+6); 
		return c.getTime();
	}

	/**
	 * @param date
	 * @return 取得日期所在周的第一天
	 */
	public static Date getFirstDayOfWeek(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		//以周一作为一周的第一天
		c.setFirstDayOfWeek(Calendar.MONDAY);
		c.set(Calendar.DAY_OF_WEEK, c.getFirstDayOfWeek());
		return c.getTime();
	}

	/**
	 * @param date
	 * @return 取得日期所在周的最后一天
	 */
	public static Date getLastDayOfWeek(Date date) {
		Calendar c = Calendar.getInstance();
		c.setFirstDayOfWeek(Calendar.MONDAY);
		c.setTime(date);
		c.set(Calendar.DAY_OF_WEEK, c.getFirstDayOfWeek() + 6); // Sunday
		return c.getTime();
	}

	/**
	 * 1： 第一季度  2： 第二季度  3： 第三季度  4： 第四季度
	 * 
	 * @param date
	 * @return 获得日期所在季度
	 */
	public static int getSeason(Date date) {
		int season = 0;
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		int month = c.get(Calendar.MONTH);
		switch (month) {
		case Calendar.JANUARY:
		case Calendar.FEBRUARY:
		case Calendar.MARCH:
			season = 1;
			break;
		case Calendar.APRIL:
		case Calendar.MAY:
		case Calendar.JUNE:
			season = 2;
			break;
		case Calendar.JULY:
		case Calendar.AUGUST:
		case Calendar.SEPTEMBER:
			season = 3;
			break;
		case Calendar.OCTOBER:
		case Calendar.NOVEMBER:
		case Calendar.DECEMBER:
			season = 4;
			break;
		default:
			break;
		}
		return season;
	}

	/**
	 * 取得月第一天
	 * 
	 * @param date
	 * @return
	 */
	public static Date getFirstDateOfMonth(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));
		return c.getTime();
	}

	/**
	 * @return 获取当前年
	 */
	public static int getCurrentYear() {
		return Calendar.getInstance().get(Calendar.YEAR);
	}
	
	/**
	 * @return 获取当前月
	 */
	public static int getCurrentMonth() {
		return Calendar.getInstance().get(Calendar.MONTH) + 1;
	}

	/**
	 * @param date
	 * @return 取得月天数
	 */
	public static int getDayNumOfMonth(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.getActualMaximum(Calendar.DAY_OF_MONTH);
	}

	/**
	 * @param date
	 * @return 取得月最后一天
	 */
	public static Date getLastDateOfMonth(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
		return c.getTime();
	}

	/**
	 * 获取当前日
	 * 
	 * @return
	 */
	public static int getCurrentDay() {
		Calendar date = Calendar.getInstance();
		int year = date.get(Calendar.DAY_OF_YEAR);
		return year;
	}
	
	/**
	 * 毫秒转化成时间日期
	 * @param milliSecond 毫秒
	 * @return
	 */
	public static Date formartDate(long milliSecond){
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(milliSecond);
		return calendar.getTime();
	}
}