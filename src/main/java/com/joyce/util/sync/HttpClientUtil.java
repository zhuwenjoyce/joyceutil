package com.joyce.util.sync;

import com.joyce.util.StrConstantUtil;
import org.apache.commons.httpclient.*;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;

import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;

import org.apache.commons.httpclient.util.URIUtil;
import org.apache.commons.lang.StringUtils;
 

import java.io.IOException;

/**
 * 发起http请求
 * @author 黄金苗
 *
 */
public class HttpClientUtil {

	/**
	 * 
	 * @param url 接口地址
	 * @param params 参数
	 * @return
	 */
 
	public static String post(String url, NameValuePair[] params) throws IOException {
		String result;
		PostMethod method = new PostMethod(url);
		try {
			method.setRequestBody(params);
			method.getParams().setContentCharset(StrConstantUtil.CHARSET_UTF8);
			HttpClient httpClient = new HttpClient();
			httpClient.executeMethod(method);
			result = method.getResponseBodyAsString();
			return result;
		}finally {
			method.releaseConnection();
		}
	}

	/**
	 * 
	 * @param url 接口地址
	 * @param parts 参数
	 * @return
	 */
 
	public static String post(String url,  Part[] parts )  throws IOException {
		String result;
		PostMethod method = new PostMethod(url);
		try {
	        MultipartRequestEntity entity = new MultipartRequestEntity(parts, method.getParams());  
	        method.setRequestEntity(entity);  
	        method.getParams().setContentCharset(StrConstantUtil.CHARSET_UTF8);
 	        HttpClient httpClient = new HttpClient();
			httpClient.executeMethod(method);
			result = method.getResponseBodyAsString();
			return result;
		} finally {
			method.releaseConnection();
		}

	}

	/** 
     * 执行一个HTTP GET请求，返回请求响应的HTML 
     * @param url 请求的URL地址 
     * @param queryString 请求的查询参数,可以为null 
     * @return 返回请求响应的HTML 
     */ 
    public static String doGet(String url, String queryString) throws IOException {
            String response = null; 
            HttpClient client = new HttpClient(); 
            HttpMethod method = new GetMethod(url); 
            try { 
                    if (StringUtils.isNotBlank(queryString)){
						method.setQueryString(URIUtil.encodeQuery(queryString));
					}
                    client.executeMethod(method);
                    if (method.getStatusCode() == HttpStatus.SC_OK) { 
                            response = method.getResponseBodyAsString(); 
                    } 
            } finally {
                    method.releaseConnection(); 
            } 
            return response; 
    } 
}
