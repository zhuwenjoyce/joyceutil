package com.joyce.util;

/**
 * 算法辅助类，包括二分算法、冒泡算法等
 * 
 * @author Joyce
 *
 */
public class AlgorithmUtil {

	/**
	 * 二分法查找，递归实现
	 * 
	 * @param intArray
	 *            源数组
	 * @param start
	 *            起始查询索引数
	 * @param end
	 *            结束查询索引数
	 * @param key
	 *            被搜索数字
	 * @return 返回数字所在索引
	 */
	public int dichotomySearch(int intArray[], int start, int end, int key) {
		int mid = (end - start) / 2 + start;
		if (intArray[mid] == key) {
			return mid;
		}
		if (start >= end) {
			return -1;
		} else if (key > intArray[mid]) {
			return dichotomySearch(intArray, mid + 1, end, key);
		} else if (key < intArray[mid]) {
			return dichotomySearch(intArray, start, mid - 1, key);
		}
		return -1;
	}

	/**
	 * 二分法查找，while循环实现
	 * 
	 * @param intArray
	 *            源数组
	 * @param key
	 *            被搜索数字
	 * @return 返回数字所在索引,如果是-1代表没有找到
	 */
	public static int dichotomySearch(int intArray[], int key) {
		int mid = intArray.length / 2;
		if (key == intArray[mid]) {
			return mid;
		}

		int start = 0;
		int end = intArray.length - 1;
		while (start <= end) {
			mid = (end - start) / 2 + start;
			if (key < intArray[mid]) {
				end = mid - 1;
			} else if (key > intArray[mid]) {
				start = mid + 1;
			} else {
				return mid;
			}
		}
		return -1;
	}
	/**
	 * 冒泡排序
	 * @param intArr 排序源数组
	 */
	public void bubbleSort(int[] intArr) {
		int temp = 0;
		for (int i = 0; i < intArr.length - 1; i++) {
			for (int j = 0; j < intArr.length - 1 - i; j++) {
				if (intArr[j] > intArr[j + 1]) {
					temp = intArr[j];
					intArr[j] = intArr[j + 1];
					intArr[j + 1] = temp;
				}
			}
		}
	}

}
