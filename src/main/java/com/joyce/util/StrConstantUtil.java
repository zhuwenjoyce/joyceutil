package com.joyce.util;



/**
 * 字符串常量类
 * @author ZhuWen
 *
 */
public class StrConstantUtil {
	/**
	 * excel .xls版本后缀
	 */
	public static final String EXCEL_XLS_POSTFIX = "xls";  
	/**
	 * excel .xlsx版本后缀
	 */
    public static final String EXCEL_XLSX_POSTFIX = "xlsx";
	/**
	 * 日期格式：YYYY-MM-DD
	 */
	public static final String DATE_FORMAT_DAY = "YYYY-MM-DD"; 
	/**
	 * 日期格式：YYYY-MM-DD HH:mm:ss
	 */
	public static final String DATE_FORMAT = "YYYY-MM-DD HH:mm:ss"; 
	/**
	 * 日期格式：YYYY-MM-DD HH:mm:ss.SSS
	 */
	public static final String DATE_FORMAT_MILLIS = "YYYY-MM-DD HH:mm:ss.SSS"; 
	
	/**
	 * 正则匹配：整数
	 */
	public static final String REG_INTEGER = "^[0-9]{1,}$";
	
	/**
	 * 正则匹配：浮点数
	 */
	public static final String REG_DOUBLE = "^[0-9]{1,}[.]{1}[0-9]{1,}$";
	/**
	 * utf-8字符编码
	 */
	public static final String CHARSET_UTF8 = "utf-8";
	/**
	 * ISO8859-1字符编码
	 */
	public static final String CHARSET_ISO8859 = "ISO8859-1";
	/**
	 * response的属性名之一："Content-disposition"，用法response.setHeader(属性名，属性值)
	 */
	public static final String RESPONSE_HEADER_CONTENT_DISPOSITION = "Content-disposition";

	/**
	 * response header "Content-disposition"的属性值："attachment; filename="，用法response.setHeader(属性名，属性值)
	 */
	public static final String RESPONSE_HEADER_CONTENT_DISPOSITION_FILENAME = "attachment; filename=";

	/**
	 * response的属性名之一："Content-Length"，用法response.setHeader(属性名，属性值)
	 */
	public static final String RESPONSE_HEADER_CONTENT_LENGTH = "Content-Length";
	/**
	 * 反斜杠
	 */
	public static final String BACK_SLANT = "/";

	/**
	 * 双斜杠
	 */
	public static final String REVERSE__SLANT = "\\";

    /**
     * 验证码字符来源
     */
    public static final String VERIFY_CODES = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

	public static final String YES = "yes";
	public static final String NO = "no";

	/**
	 * 文本类型html
 	 */
	public static final String TEXT_TYPE_HTML = "html";

}
