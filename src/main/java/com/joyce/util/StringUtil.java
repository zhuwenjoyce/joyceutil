package com.joyce.util;


import java.util.Arrays;
import java.util.List;

/**
 * 在StringUtils类(commons-lang)上做扩展
 * @author ZhuWen
 *
 */
public class StringUtil{

	/**
	 * @param string
	 * @return 字符串转换为整数
	 */
	public static Integer toInteger(String string){
		return Integer.valueOf(string);
	}

	/**
	 * @param string
	 * @return 字符串转换为浮点数double
	 */
	public static Double toDouble(String string){
		return Double.valueOf(string);
	}

	/**
	 * @param string
	 * @return 字符串转换为浮点数float
	 */
	public static Float toFloat(String string){
		return Float.valueOf(string);
	}
	
	/**
	 * @param string 接收多个不定长度参数
	 * @return 返回所有参数的数组
	 */
	public static String[] toArray(String... string){
		return string;
	}
	
	/**
	 * @param string 接收多个不定长度参数
	 * @return 返回包含所有参数的list列表
	 */
	public static List<String> toList(String... string){
		return Arrays.asList(string);
	}
	
	/**
	 * @param strArr 字符串数组
	 * @param separator 分隔符
	 * @return 以指定分隔符拼接包含所有字符串数组中对象的字符串
	 */
	public static String join(String[] strArr, String separator){
		StringBuffer buffer = new StringBuffer();
		int len = strArr.length;
		for (int i = 0; i < len; i++) {
			buffer.append(strArr[i]);
			if(i != len-1){
				buffer.append(separator);
			}
		}
		return buffer.toString();
	}
}
