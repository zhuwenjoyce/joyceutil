package com.joyce.util.excel;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * excel导入辅助类,仅支持xlsx格式，暂不支持单元格合并等复杂格式
 * 
 * @author ZhuWen
 *
 */
public class ExcelImportUtil {

	/**
	 * 封装每一行数据到一个数组，所有的行数组封装成一个二维数组
	 * 
	 * @param excelPath
	 *            excel所在目录，绝对路径例如d:/abc.xlsx
	 * @param startRownum
	 *            第几个sheet页，默认索引从0开始
	 * @param startRownum
	 *            起始读入行
	 * @return Object[][]  封装每一行数据到一个数组，所有的行数组封装成一个二维数组
	 * @throws IOException
	 */
	@SuppressWarnings({"resource" })
	public Object[][] importExcel(String excelPath, int sheetNum,
			int startRownum) throws IOException {
		InputStream ips = new FileInputStream(excelPath);
		XSSFWorkbook wb = new XSSFWorkbook(ips);
		XSSFSheet sheet = wb.getSheetAt(sheetNum);
		
		//获得总行数
		int rowTotalNum = sheet.getLastRowNum() +1;
		//获得总列数，根据标题行得到的列数一般都是准确的
		int coloumTotalNum = sheet.getRow(0).getPhysicalNumberOfCells(); 
		//返回的数据结果集，包含了所有行的所有列的数据
		Object[][] resultData = new Object[rowTotalNum][coloumTotalNum]; 
		
		//遍历每行获取数据
		Iterator<Row> rowIt = sheet.rowIterator();
		int rowNumCount = 0;
		while(rowIt.hasNext()){
			Row row = rowIt.next();
			Iterator<Cell> cellIt = row.cellIterator();
			int columnNumCount = 0;
			while(cellIt.hasNext()){
				Cell cell = cellIt.next();
				cell.getCellType();
				switch (cell.getCellType()) {
					case XSSFCell.CELL_TYPE_BLANK:
						resultData[rowNumCount][columnNumCount] = "";
						break;
					case XSSFCell.CELL_TYPE_NUMERIC:
						resultData[rowNumCount][columnNumCount] = cell.getNumericCellValue();
						break;
					default:
						resultData[rowNumCount][columnNumCount] = cell.getStringCellValue();
						break;
				}
				columnNumCount++;
			}
			rowNumCount++;
		}

		return resultData;
	}
}
