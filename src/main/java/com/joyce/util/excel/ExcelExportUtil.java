package com.joyce.util.excel;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.joyce.util.DateUtil;
import com.joyce.util.StrConstantUtil;
/**
 * excel导出辅助类,仅支持xlsx格式，暂不支持单元格合并等复杂格式
 * @author ZhuWen
 *
 */
public class ExcelExportUtil {

	
	/**
	 * 导出xlsx格式的excel，默认生成一个sheet页，会强制覆盖输出文件
	 * @param outputStream  支持多种途径的输出方式，如OutputStream outputStream = response.getOutputStream(); 或者 OutputStream outputStream = new FileOutputStream(file);
	 * @param sheetName     sheet页名字
	 * @param headerDataArray   标题行数据
	 * @param startRownum       数据起始写入行，起始值为0
	 * @param dataArray         数据源集合
	 * @throws IOException
	 */
	public void exportExcel(OutputStream outputStream, String sheetName, Object[][] headerDataArray, int startRownum, Object[][] dataArray) throws IOException{
		@SuppressWarnings("resource")
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet(sheetName); 
		workbook.setSheetName(0, sheetName);
		
		//写入标题行
		writeCell(headerDataArray,sheet,0);
		//写入数据行
		writeCell(dataArray,sheet,startRownum);
		
		//写入到excel
		workbook.write(outputStream);
		outputStream.flush();
		outputStream.close();
	}
	/**
	 * 提取出来的公用代码，写入数据到单元格
	 * @param dataArray    数据源
	 * @param sheet        写入到sheet页
	 * @param startRownum  起始写入行
	 */
	private void writeCell(Object[][] dataArray, XSSFSheet sheet, int startRownum){
		//如果数据集不为空，才有导出的必要
		if(dataArray != null && dataArray.length > 0){
			for (int line = 0; line < dataArray.length; line++) {
				Object[] dataArr = dataArray[line];
				Row row = sheet.createRow(startRownum+line);
				if(dataArr != null && dataArr.length > 0){
					for (int columnIndex = 0; columnIndex < dataArr.length; columnIndex++) {
						Object data = dataArr[columnIndex];
						Cell cell = row.createCell(columnIndex); 
						if(data instanceof Double ){
							cell.setCellValue((Double)data);   //浮点数
						}else if(data instanceof Integer){
							cell.setCellValue((Integer)data);   //整数
						}else if(data instanceof Date){
							cell.setCellValue(DateUtil.formatToString((Date)data,StrConstantUtil.DATE_FORMAT_DAY));   //日期数据
						}else{
							cell.setCellValue((String)data);   //字符串
						}
					}
				}
			}
		}
	}
}
