package com.joyce.util;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class StringUtilTest {
	@Test
	public void toInteger() throws Exception {
		assertEquals(11,StringUtil.toInteger("11").intValue());
	}

	@Test
	public void toDouble() throws Exception {
       assertEquals(3.25,StringUtil.toDouble("3.25"),3);
	}

	@Test
	public void toFloat() throws Exception {
		assertEquals(4.0f,StringUtil.toFloat("4.0f"),4.0f);
	}

	@Test
	public void toArray() throws Exception {
        assertArrayEquals(new String[]{"1","2","3"},StringUtil.toArray("1","2","3"));
	}

	@Test
	public void toList() throws Exception {
		List<String> list = StringUtil.toList("1","2","3");
		assertEquals(3, list.size());
	}

	@Test
	public void join() throws Exception {
       assertEquals("1,2,3",StringUtil.join(new String[]{"1","2","3"},","));
       assertEquals("1-2-3",StringUtil.join(new String[]{"1","2","3"},"-"));
	}
}
