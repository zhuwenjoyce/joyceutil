package com.joyce.util.sync;

import java.io.File;
import com.joyce.util.StrConstantUtil;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.methods.multipart.StringPart;
import org.junit.Test;

 
public class HttpClientUtilTest {
	
	public static String parameter="parameter";

	public static String url="http://139.224.44.215/snapshot/crack";

	//文件地址
	public static String filePath="E://5243d1fd4f36448ab86ec7d3b123d5b8.jpg";

	 
	@Test
	public void testPostStringNameValuePairArray() {
		try{
			NameValuePair[] nameValuePairs
					//属性名称，属性值
					= new NameValuePair[]{new NameValuePair(parameter, parameter)};
			String response = HttpClientUtil.post(url, nameValuePairs);
			System.out.println(response);
		}catch (Exception e){

		}
 	}

	@Test
	public void testPostStringPartArray() {
		try {
			Part[] parts = new Part[]{   
					//属性名称，属性值，属性格式
	                new StringPart(parameter,parameter,StrConstantUtil.CHARSET_UTF8),
	                //file表示传递的文件
	                new FilePart("file", new File(filePath))};
	   	   String result=HttpClientUtil.post(url, parts);
	   	 System.out.println(result);
		} catch (Exception e) {
		  System.out.print(e);
		}
	}
	
	
	@Test
	public void testDoGet() {
		try{
			HttpClientUtil.doGet(url,null);
		}catch (Exception e){

		}
	}
}
