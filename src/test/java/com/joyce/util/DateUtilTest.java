package com.joyce.util;

import org.junit.Test;
import com.joyce.util.StrConstantUtil;
import java.util.Date;
import static org.junit.Assert.assertEquals;

public class DateUtilTest {

	@Test
	public void formatCurrentDate() throws Exception {
       assertEquals("2017-01-18",DateUtil.formatCurrentDate(StrConstantUtil.DATE_FORMAT_DAY));
       assertEquals("2017/01/18",DateUtil.formatCurrentDate("yyyy/MM/dd"));
	}

	@Test
	public void formatToString() throws Exception {
		assertEquals("2017/01/18",DateUtil.formatToString(new Date(),"yyyy/MM/dd"));
	}

	@Test
	public void parseToDate() throws Exception {
		assertEquals("2017/01/18",DateUtil.parseToDate("2017/01/18","yyyy/MM/dd"));
	}

	@Test
	public void toChineseDate() throws Exception {
		assertEquals("二〇一七年一月十八日",DateUtil.toChineseDate("2017-01-18"));
	}

	@Test
	public void getWeekOfYear() throws Exception {
        assertEquals(3,DateUtil.getWeekOfYear(new Date()));
	}

	@Test
	public void getTotalWeekNumOfYear() throws Exception {
		assertEquals(52,DateUtil.getTotalWeekNumOfYear(2016));
	}

	@Test
	public void getFirstDayOfWeek() throws Exception {
		assertEquals(1,DateUtil.getFirstDayOfWeek(2017,1));
	}

	@Test
	public void getLastDayOfWeek() throws Exception {
       assertEquals(1,DateUtil.getLastDayOfWeek(2017,1));
	}

	@Test
	public void getFirstDayOfWeek1() throws Exception {
      assertEquals(1,DateUtil.getFirstDayOfWeek(new Date()));
	}

	@Test
	public void getLastDayOfWeek1() throws Exception {
		assertEquals(1,DateUtil.getLastDayOfWeek(new Date()));
	}

	@Test
	public void getSeason() throws Exception {
		assertEquals(1,DateUtil.getSeason(new Date()));
	}

	@Test
	public void getFirstDateOfMonth() throws Exception {
       assertEquals(1,DateUtil.getFirstDateOfMonth(new Date()));
	}

	@Test
	public void getCurrentYear() throws Exception {
       assertEquals(2017,DateUtil.getCurrentYear());
	}

	@Test
	public void getCurrentMonth() throws Exception {
       assertEquals(1,DateUtil.getCurrentMonth());
	}

	@Test
	public void getDayNumOfMonth() throws Exception {
     assertEquals(31,DateUtil.getDayNumOfMonth(new Date()));
	}

	@Test
	public void getLastDateOfMonth() throws Exception {
      assertEquals(1,DateUtil.getLastDateOfMonth(new Date()));
	}

	@Test
	public void getCurrentDay() throws Exception {
      assertEquals(18,DateUtil.getCurrentDay());
	}

	@Test
	public void formartDate() throws Exception {
		assertEquals(1,DateUtil.formartDate(1484709367282L));
	}
}
