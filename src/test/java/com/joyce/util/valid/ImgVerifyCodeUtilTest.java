package com.joyce.util.valid;

import com.joyce.util.valid.ImgVerifyCodeUtil;
import org.junit.Test;
import java.io.File;
import java.io.FileOutputStream;

public class ImgVerifyCodeUtilTest {

    @Test
    public void test() throws Exception {
    	for (int i = 0; i < 10; i++) {
    		int w = 200, h = 80;
            //用指定的源生成code
            String verifyCode = ImgVerifyCodeUtil.generateVerifyCode(6, "1234567890");
            File file = new File("d:/c"+verifyCode+".jpg"); //以验证码为图片命名，方便直观测试
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            ImgVerifyCodeUtil.outputImage(w, h, fileOutputStream, verifyCode,"png");
		}
        
    }
}