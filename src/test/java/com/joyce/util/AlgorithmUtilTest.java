package com.joyce.util;

import static org.junit.Assert.*;
import org.junit.Test;
import com.alibaba.fastjson.JSONObject;

public class AlgorithmUtilTest {

	@Test
	public void testDichotomySearch() {
		int[] intArr = {3,5,11,17,21,23,28,30,32,50,64,78,81,95,101}; 
		AlgorithmUtil algo = new AlgorithmUtil();
		int index = algo.dichotomySearch(intArr,0,intArr.length-1, 21);
		assertEquals(4, index);//使用断言
	}

	@Test
	public void testDichotomySearch2() {
		int[] intArr = {3,5,11,17,21,23,28,30,32,50,64,78,81,95,101}; 
		int index = AlgorithmUtil.dichotomySearch(intArr, 21);
		assertEquals(4, index);
	}

	@Test
	public void testBubbleSort() {
		int[] intArr = { 49, 38, 65, 97, 76, 13, 27, 49, 78, 34, 12, 64, 5, 4,
				62, 99, 98, 54, 56, 17, 18, 23, 34, 15, 35, 25, 53, 51 };
		AlgorithmUtil algo = new AlgorithmUtil();
		algo.bubbleSort(intArr);
		//打印一下看数据排序情况
		System.out.println(JSONObject.toJSONString(intArr));
		assertEquals(4, intArr[0]);
		assertEquals(99, intArr[intArr.length-1]);
	}

}
