package com.joyce.util.parse;

import java.io.IOException;
import org.junit.Before;
import org.junit.Test;

import com.joyce.util.parse.ImagesToPdfUtil;
import com.itextpdf.text.DocumentException;

public class ImagesToPdfUtilTest {

	public static final String image_file_one="E://dc50a4958fff4cca9a83bd75bc75af40.jpg";

	public static final String image_file_two="E://5243d1fd4f36448ab86ec7d3b123d5b8.jpg";

	public static final String pdf_file="E://fd4f36448ab86ec7d3b1.pdf";

	/**
	 * 截取图片存放地址
	 */
	public static final String parser_image_path="E://";

	/**
	 * 生成pdf的页数
	 */
	public static final int  sum=2;

	/**
	 * 截取图片是否删除，true删除，false保留
	 */
	public static   boolean  state =false;


	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testImagesToPdfOne() throws IOException, DocumentException {
		ImagesToPdfUtil imagesToPdfUtil=new ImagesToPdfUtil();
		imagesToPdfUtil.imagesToPdfOne(image_file_one,pdf_file );
	}

	@Test
	public void testImagesToPdfMore() throws IOException, DocumentException{
		ImagesToPdfUtil imagesToPdfUtil=new ImagesToPdfUtil();
		imagesToPdfUtil.imagesToPdfMore(image_file_two, pdf_file, parser_image_path, sum,state);
	}

}
