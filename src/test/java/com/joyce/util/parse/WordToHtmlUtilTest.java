package com.joyce.util.parse;

import java.io.File;
import com.joyce.util.StrConstantUtil;
import org.junit.Test;

import com.joyce.util.parse.WordToHtmlUtil;

public class WordToHtmlUtilTest {

	/**
	 *doc文件流
	 */
	public static String   FileNameDoc="D://word/83.doc";

	/**
	 *docx文件流
	 */
	public static String   FileNameDocx="D://word/83.docx";

	/**
	 *targetFileName  html文件路径
	 */
	public static String   targetFileName="D://word/83.html";


	/**
	 * 2003word转html
	 */
	@Test
	public void testDocToHtml() {
		WordToHtmlUtil wordToHtmlUtil=new WordToHtmlUtil();
		try {
			wordToHtmlUtil.docToHtml(new File(FileNameDoc), targetFileName,StrConstantUtil.CHARSET_UTF8);
		}catch (Exception x){
			System.out.print(x.toString());
		}
	}
	/**
	 * 2007以上版本转html
	 */
	 @Test
	public void testDocxToHtml() {
	 WordToHtmlUtil wordToHtmlUtil=new WordToHtmlUtil();
		 try {
			 wordToHtmlUtil.docxToHtml (new File(FileNameDocx), targetFileName,StrConstantUtil.CHARSET_UTF8);
		 }catch (Exception x){
			 System.out.print(x.toString());
		 }
	}
}
