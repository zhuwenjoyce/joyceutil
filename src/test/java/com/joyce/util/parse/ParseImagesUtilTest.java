package com.joyce.util.parse;

import java.io.IOException;
import org.junit.Test;

import com.joyce.util.parse.ParseImagesUtil;

public class ParseImagesUtilTest {


	/**
	 * 剪切点x坐标
	 */
	public static int x =0;

	/**
	 * 剪切点y坐标
	 */
	public static int y=0;

	/**
	 * 剪切点宽度
	 */
	public static int width=5000;

	/**
	 * 剪切点高度
	 */
	public static int height=1444;

	/**
	 * 源图片路径名称如:c:\1.jpg
	 */
	public static String srcpath = "e:/5243d1fd4f36448ab86ec7d3b123d5b8.jpg";
	/**
	 * 剪切图片存放路径名称.如:c:\2.jpg
	 */
	public static String subpath = "e:/890.jpg";

	@Test
	public void testCut() throws IOException {
		ParseImagesUtil imageCut = new ParseImagesUtil();
        imageCut.cut(srcpath, subpath,x, y, width, height);
	}

}
