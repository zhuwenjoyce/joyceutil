package com.joyce.util.excel;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.alibaba.fastjson.JSONObject;

public class ExcelImportUtilTest {
	
	private ExcelImportUtil excelImportUtil = new ExcelImportUtil();

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testImportExcel() throws Exception {
		Object[] obj = new Object[2];
		obj[0] = new Object[10];
		obj[1] = new Object[20];
		
		Object[][] dataResult = excelImportUtil.importExcel("d:/abc.xlsx", 0, 1);
		System.out.print(JSONObject.toJSONString(dataResult));
	}

}
