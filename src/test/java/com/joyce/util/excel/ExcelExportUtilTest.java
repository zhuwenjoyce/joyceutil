package com.joyce.util.excel;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

public class ExcelExportUtilTest {
	
	private ExcelExportUtil excelExportUtil = new ExcelExportUtil();

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testExportExcel() throws IOException {
		File file = new File("d:/abc.xlsx");
		OutputStream outputStream = new FileOutputStream(file);
		
		//标题行
		Object[][] headerDataArray = new Object[1][5];
		Object[] headerDataArray1 = new Object[5];
		headerDataArray1[0] = "标题列一";
		headerDataArray1[1] = "标题列二";
		headerDataArray1[2] = "标题列三";
		headerDataArray1[3] = "标题列四";
		headerDataArray1[4] = "标题列五";
		headerDataArray[0] = headerDataArray1;
		
		//数据行
		Object[][] dataArray = new Object[2][5];
		Object[] dataArray1 = new Object[5];
		dataArray1[0] = "数据一";
		dataArray1[1] = "数据二";
		dataArray1[2] = "数据三";
		dataArray1[3] = 33.55d;
		dataArray1[4] = 100;
		dataArray[0] = dataArray1;
		Object[] dataArray2 = new Object[5];
		dataArray2[0] = "张三";
		dataArray2[1] = "李四";
		dataArray2[2] = "订单";
		dataArray2[3] = new Date();
		dataArray2[4] = 60;
		dataArray[1] = dataArray2;
		
		excelExportUtil.exportExcel(outputStream,"seetA", headerDataArray,1,dataArray);
	}

}
