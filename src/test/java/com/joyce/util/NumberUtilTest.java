package com.joyce.util;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class NumberUtilTest {
	private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory
			.getLogger(NumberUtilTest.class);

	@Before
	public void setUp() {
	}

	@Test
	public void test() {
		String string = " 3";
		boolean boo = NumberUtil.isInteger(string);
		LOG.info(boo+"");
		
		Integer value = null;
		String str = NumberUtil.toString(value);
		LOG.info(str);
	}

	@Test
	public void randomInteger() throws Exception {
		System.out.println(NumberUtil.randomInteger(100, 10, 5));
		/*Integer[] integers = NumberUtil.randomInteger(10, 100, 5);
		for (Integer i : integers){
			System.out.println(i);
		}*/
	}

	@Test
	public void idDouble() throws Exception {
		assertEquals(true,NumberUtil.isDouble("3.22"));
		assertEquals(false,NumberUtil.isDouble(" 3.22"));
		assertEquals(false,NumberUtil.isDouble("12"));
	}

	@Test
	public void isNumber() throws Exception {
       assertEquals(true,NumberUtil.isNumber("12.33"));
       assertEquals(true,NumberUtil.isNumber("12"));
       assertEquals(false,NumberUtil.isNumber("12."));
       assertEquals(false,NumberUtil.isNumber("12--"));
       assertEquals(false,NumberUtil.isNumber(" 12"));
	}

	@Test
	public void isPrimes() throws Exception {
       assertEquals(true,NumberUtil.isPrimes(3));
       assertEquals(false,NumberUtil.isPrimes(4));
       assertEquals(true,NumberUtil.isPrimes(1));
	}

	@Test
	public void sortAscending() throws Exception {
		int[] i = {1, 5, 10, 2, 7, 9, 3};
		NumberUtil.sortAscending(i);
		assertArrayEquals(new int[]{1,2,3,5,7,9,10},i);
	}

	@Test
	public void sortDescending() throws Exception {
		int[] i = {1, 5, 10, 2, 7, 9, 3};
		NumberUtil.sortDescending(i);
		assertArrayEquals(new int[]{10,9,7,5,3,2,1},i);
	}

	@Test
	public void factorial() throws Exception {
		assertEquals(2, NumberUtil.factorial(2));
		assertEquals(24, NumberUtil.factorial(4));
	}

	@Test
	public void sqrt() throws Exception {
     assertEquals(10,NumberUtil.sqrt(100));
     assertEquals(3,NumberUtil.sqrt(9));
	}

	@Test
	public void maxCommonDivisor() throws Exception {
      assertEquals(10,NumberUtil.maxCommonDivisor(10,100));
      assertEquals(4,NumberUtil.maxCommonDivisor(0,4));
	}

	@Test
	public void minCommonMultiple() throws Exception {
		assertEquals(100,NumberUtil.minCommonMultiple(10,100));
		assertEquals(0,NumberUtil.minCommonMultiple(10,0));
	}

	@Test
	public void recursionMaxCommonDivisor() throws Exception {
       assertEquals(10,NumberUtil.recursionMaxCommonDivisor(10,100));
	}
}
